var Component = require('./Component'),
  util = require('util'),
  _ = require('underscore');

var HtmlComponent = function (name, data, options) {
  options = _.defaults(options || {}, {attributes: {}});
  Component.call(this, name, data, options);
};

util.inherits(HtmlComponent, Component);

HtmlComponent.prototype.prerender = function() {

};

HtmlComponent.prototype.test = function() {};

module.exports = HtmlComponent;
