var path = require('path'),
  dsign_util = require('./util'),
  Twig = require('./twig'),
  twig = Twig.twig,
  util = require('util');

var Template = function(file, data) {
  this.file = file;
  this.data = data;
};

/**
 * Create Twig template object out of template file.
 */
Template.prototype.getTemplate = function() {
  if (dsign_util.fileExists(this.file)) {
    return twig({
        path: this.file,
        async: false
    });
  }
  else {
    process.stderr.write(util.format("Template file '%s' does not exist for component", this.name));
  }
};

Template.prototype.render = function() {
  var template = this.getTemplate();
  if (template) {
    return template.render({
      data: this.data,
    });
  }
};

module.exports = Template;
