

var path = require('path'),
  dsign_util = require('../util'),
  prettyPrint = require('html/lib/html').prettyPrint,
  _ = require('underscore'),
  fs = require('fs');

var build = function(options) {
  var inputfile = options.i || options.input;
  if (!inputfile) {
    process.stderr.write("Input file is not specified.\n");
    process.exit(1);
  }

  var Renderer = require('../../lib/Renderer');
  var renderer = Renderer.getRenderer();
  var file_extension = path.extname(inputfile);
  if (['.yml', '.yaml'].indexOf(file_extension.toLowerCase()) > -1) {
    component_data = dsign_util.readYamlFile(inputfile);
  }
  else {
    component_data = require(inputfile);
  }
  process.stdout.write((prettyPrint(renderer.render(component_data), {indent_size: 1, indent_char: ' '})));

  var attachment_outputfiles = {};
  var attachment_outputfiles_count = 0;
  var attachment_output_option_pattern = /^out\:(\w+)/;
  for (var option in options) {
    var result = attachment_output_option_pattern.exec(option);
    if (Array.isArray(result)) {
      attachment_outputfiles[result[1]] = options[option];
      attachment_outputfiles_count++;
    }
  }
  if (attachment_outputfiles_count) {
    var attachments = renderer.getAttachements();
    _.each(attachment_outputfiles, function(file, type){
      if (attachments[type]) {

        fs.writeFileSync(path.resolve(process.cwd(), file), attachments[type], {encoding: 'utf8'} );
      }
    });
  }
};

module.exports = build;
