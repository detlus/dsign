

var path = require('path'),
  dsign_util = require('../util'),
  prettyPrint = require('html/lib/html').prettyPrint,
  _ = require('underscore'),
  fs = require('fs');

var build = function(options) {
  var dsignfile = JSON.parse(fs.readFileSync('dsignfile.json', 'utf8'));

  _.each(dsignfile.build, function(outputfile, inputfile){
    var Renderer = require('../../lib/Renderer');
    var renderer = Renderer.getRenderer();
    var file_extension = path.extname(inputfile);
    if (['.yml', '.yaml'].indexOf(file_extension.toLowerCase()) > -1) {
      component_data = dsign_util.readYamlFile(inputfile);
    }
    else {
      component_data = require(inputfile);
    }
    // process.stdout.write(prettyPrint(renderer.render(component_data), {indent_size: 1, indent_char: ' '}));
    fs.writeFileSync(
      outputfile,
      prettyPrint(renderer.render(component_data), {indent_size: 1, indent_char: ' '}),
      {encoding: 'utf8'}
    );
  });

  console.log(dsignfile);
};

module.exports = build;
