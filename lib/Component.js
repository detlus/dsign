var path = require('path'),
  dsign_util = require('./util'),
  Twig = require('./twig'),
  twig = Twig.twig;

var Component = function(name, data, options) {
  this.name = name;
  this.data = data;
  options = options || {};
  this.options = options;
};

Component.prototype.getTemplate = function() {
  var template_path;
  var custom_template_path = path.join(process.cwd(), 'component-types', this.name, 'template.twig');
  var custom_template_path2 = path.join(process.cwd(), 'component-types', this.name + '.twig');
  var builtin_template_path = path.join(__dirname, '..', 'component-types', this.name, 'template.twig');
  if (dsign_util.fileExists(custom_template_path)) {
    template_path = custom_template_path;
  }
  else if(dsign_util.fileExists(custom_template_path2)) {
    template_path = custom_template_path2;
  }
  else if(dsign_util.fileExists(builtin_template_path)) {
    template_path = builtin_template_path;
  }
  else {
    process.stderr.write("Template does not exist for component '" + this.name + "'");
  }
  if (dsign_util.fileExists(template_path)) {
    return twig({
        path: template_path,
        async: false
    });
  }
};

Component.prototype.prerender = function() {};

Component.prototype.render = function() {
  var template = this.getTemplate();
  if (template) {
    this.prerender();
    return template.render({
      data: this.data,
      options: this.options
    });
  }
};

module.exports = Component;
