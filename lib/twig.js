var Twig = require('twig'),
  _ = require('underscore'),
  Renderer = require('./Renderer'),
  dsign_util = require('./util');

// // Replace Twig's default date filter.
// Twig.extendFilter("date", function(value, params) {
//   if (_.isUndefined(params)) {
//     return moment(value).format();
//   }
//   else {
//     return moment(value).format(params[0]);
//   }
//
// });

Twig.extendFunction("render", function(data) {
  return Renderer.getRenderer().render(data);
});

Twig.extendFunction("print_attributes", function(attributes) {
  return dsign_util.printAttributes(attributes);
});

module.exports = Twig;
