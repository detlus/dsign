var _ = require('underscore'),
  dsign_util = require('./util'),
  path = require('path'),
  fs = require('fs');

var Renderer = function () {
  this.attachments = {};
};

Renderer.renderer = null;

Renderer.getRenderer = function() {
  if (!Renderer.renderer) {
    Renderer.renderer = new Renderer();
  }
  return Renderer.renderer;
};

Renderer.prototype.getComponentHandler = function(name) {
  var handler_path;
  var custom_handler_path = path.join(process.cwd(), 'component-types', name, 'handler.js');
  var builtin_handler_path = path.join(__dirname, '..', 'component-types', name, 'handler.js');
  if (dsign_util.fileExists(custom_handler_path)) {
    handler_path = custom_handler_path;
  }
  else if(dsign_util.fileExists(builtin_handler_path)) {
    handler_path = builtin_handler_path;
  }
  else {
    handler_path = path.join(__dirname, 'HtmlComponent');
  }
  return require(handler_path);
};

Renderer.prototype.getTemplate = function(file, data) {
  var Template = require('./Template');
  return new Template(file, data || {});
};

Renderer.prototype.addAttachement = function(type, file) {
  if (!this.attachments[type]) {
    this.attachments[type] = [];
  }
  this.attachments[type].push(file);
};
Renderer.prototype.getAttachements = function() {
  var outputs = {};
  _.each(this.attachments, function(attachments, type){
    outputs[type] = '';
      _.each(attachments, function(attachment){
        if (attachment) {
          outputs[type] += "\n" + fs.readFileSync(attachment, {encoding: 'utf8'});
        }
      });
  });
  return outputs;
};

Renderer.prototype.collectAttachments = function(data) {
  var that = this;
  if (data['#attachments']) {
    _.each(data['#attachments'], function(attachments, type){
      _.each(attachments, function(attachment){
        that.addAttachement(type, attachment);
      });
    });
  }
};

Renderer.prototype.render = function(data) {
  var that = this;
  var file_extension;
  var data_type = typeof data;
  if (data_type === 'string' || _.isNull(data)) {
    return data;
  }
  else if (data_type === 'object') {
    if (data['#component']) {
      var component_class = this.getComponentHandler(data['#component']);
      if (component_class) {
        var component_data = null;
        if (!_.isUndefined(data['#data']) && !_.isNull(data['#data'])) {
          component_data = data['#data'];
        }
        else if (data['#file']) {
          if (dsign_util.fileExists(data['#file'])) {
            file_extension = path.extname(data['#file']);
            if (['.yml', '.yaml'].indexOf(file_extension.toLowerCase()) > -1) {
              component_data = dsign_util.readYamlFile(data['#file']);
            }
            else {
              component_data = require(data['#file']);
            }
          }
          else {
            process.stderr.write('File', data['#file'], 'does not exist.');
          }
        }
        var component = new component_class(data['#component'], component_data, data['#options'] || {});
        this.collectAttachments(data);
        return component.render();
      }
    }
    else if (data['#file']) {
      if (dsign_util.fileExists(data['#file'])) {
        file_extension = path.extname(data['#file']);
        if (['.twig'].indexOf(file_extension.toLowerCase()) > -1) {
          var template = this.getTemplate(data['#file'], data['#data'] || {});
          return template.render();
        }
        else if (['.yml', '.yaml'].indexOf(file_extension.toLowerCase()) > -1) {
          return this.render(dsign_util.readYamlFile(data['#file']));
        }
        else {
          return fs.readFileSync(data['#file'], {encoding: 'utf8'});
        }
      }
      else {
        process.stderr.write('File', data['#file'], 'does not exist.');
        console.log('File', data['#file'], 'does not exist.');
      }
    }
  }
};

module.exports = Renderer;
