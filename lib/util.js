var fs = require('fs'),
  yaml = require('js-yaml'),
  util = require('util');

var fileExists = function(path) {
  var file_exists = false;
  try{
    fs.statSync(path);
    file_exists = true;
  }
  catch (e){
  }
  return file_exists;
};

var readYamlFile = function(filepath) {
  var info = null;
  var file_exists = false;
  try{
    fs.statSync(filepath);
    file_exists = true;
  }
  catch (e){

  }
  if (file_exists) {
    try {
      // Read YAML data from the file.
      info = yaml.safeLoad(fs.readFileSync(filepath, 'utf8'));
    }
    catch (e) {

    }
  }
  return info;
};

var printAttributes = function(attributes) {
  if (typeof attributes === 'object') {
    var output = [];
    for (var attribute in attributes) {
      if (attribute === 'class') {
        if (Array.isArray(attributes[attribute])) {
          output.push(util.format('%s="%s"', attribute, attributes[attribute].join(' ')));
        }
        else {
          throw new Error('HTML classes to be specified in array.');
        }
      }
      else {
        output.push(util.format('%s="%s"', attribute, attributes[attribute]));
      }
    }
    return util.format(' %s ', output.join(' '));
  }
  else {
    throw new Error('Attributes must be an object.');
  }
};

module.exports = {
  fileExists: fileExists,
  readYamlFile: readYamlFile,
  printAttributes: printAttributes
};
